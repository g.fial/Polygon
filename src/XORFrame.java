import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
/**
 * @author Guilherme Fial 42210
 * @author Frederico Alves 41713
 */

public class XORFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private static final String BEZIER = "Bezier";
	private static final String B_SPLINE = "B-Spline";
	private static final String CATMULL = "Catmull-Rom";
	
	public XORFrame(){
		setSize(WIDTH, HEIGHT);

		JMenuBar barra = new JMenuBar();
		barra.add(criarMenuFicheiro());
		barra.add(criarMenuView());
		barra.add(createOptionsMenu());
		barra.add(criarMenuAjuda());
		setJMenuBar(barra);
		
		painel = new XORPanel();
		setContentPane(painel);
	}
	
	private JMenu criarMenuFicheiro() {
		JMenu menu = new JMenu("File");
		menu.add(createFilePrint("Print"));
		menu.addSeparator();
		menu.add(createFileExit("Exit"));
		return menu;
	}
	
	private JMenu criarMenuView() {
		JMenu viewMenu = new JMenu("View");
		JMenu curveMenu = new JMenu("Curves");
		
		final JCheckBoxMenuItem bezier = new JCheckBoxMenuItem(BEZIER);
		final JCheckBoxMenuItem bspline = new JCheckBoxMenuItem(B_SPLINE);
		final JCheckBoxMenuItem catmull = new JCheckBoxMenuItem(CATMULL);
		
		class ListenerviewMenu implements ActionListener
		{
			public void actionPerformed(ActionEvent event)
			{
				
				if(event.getActionCommand().equals(BEZIER)) {
					painel.toggleBezier();
				}
				
				else if(event.getActionCommand().equals(B_SPLINE)) {
					painel.toggleBSpline();
				}
				
				else if(event.getActionCommand().equals(CATMULL)) {
					painel.toggleCatmull();
				}
			}
		}
		
		bezier.addActionListener(new ListenerviewMenu());
		bspline.addActionListener(new ListenerviewMenu());
		catmull.addActionListener(new ListenerviewMenu());
		
		curveMenu.add(bezier);
		curveMenu.add(bspline);
		curveMenu.add(catmull);
		viewMenu.add(curveMenu);
		viewMenu.add(criarViewPolLine("Polygonal Line"));
		viewMenu.add(new JSeparator());
		viewMenu.add(criarViewBB("Bounding-Box"));
		
		return viewMenu;
	}
	
	private JMenuItem createFileExit(String texto)
	{
		JMenuItem item = new JMenuItem(texto);
		
		class ListenerItemMenu implements ActionListener
		{

			public void actionPerformed(ActionEvent e) {
				
				System.exit(0);
			}	
		}
		
		item.addActionListener(new ListenerItemMenu());
		return item;	
	}
	
	private JMenuItem createFilePrint(String texto)
	{
		JMenuItem item = new JMenuItem(texto);
		
		class ListenerItemMenu implements ActionListener
		{

			public void actionPerformed(ActionEvent e) {
				
				painel.startPrinting();
			}	
		}
		
		item.addActionListener(new ListenerItemMenu());
		return item;	
	}
	
	private JMenu createOptionsMenu()
	{
		JMenu optionsMenu = new JMenu("Options");
		JMenu pointsMenu = new JMenu("Control Points");
		
		final JCheckBoxMenuItem modo4 = new JCheckBoxMenuItem("4 Points");
		final JCheckBoxMenuItem modo7 = new JCheckBoxMenuItem("7 Points");
		final JCheckBoxMenuItem modo10 = new JCheckBoxMenuItem("10 Points");
		final JMenuItem reset = new JMenuItem("Reset");
		
		class ListenerOptionsMenu implements ActionListener
		{
			public void actionPerformed(ActionEvent event)
			{
				
				if(event.getActionCommand().equals("Reset")) {
					painel.clear();
					
				}
				
				else if(event.getActionCommand().equals("4 Points")) {
					modo7.setSelected(false);
					modo10.setSelected(false);
					enableOtherChoices(modo4);
					painel.setSequence(4);
				}
				
				else if(event.getActionCommand().equals("7 Points")) {
					modo4.setSelected(false);
					modo10.setSelected(false);
					enableOtherChoices(modo7);
					painel.setSequence(7);
				}
				
				else if(event.getActionCommand().equals("10 Points")) {
					modo4.setSelected(false);
					modo7.setSelected(false);
					enableOtherChoices(modo10);
					painel.setSequence(10);
				}
			}
			
			private void enableOtherChoices(JMenuItem item) {
				
				item.setEnabled(false);
				
				if(!item.equals(modo4))
					modo4.setEnabled(true);
				
				if(!item.equals(modo7))
					modo7.setEnabled(true);
				
				if(!item.equals(modo10))
					modo10.setEnabled(true);
			}
		}
		
		modo4.addActionListener(new ListenerOptionsMenu());
		modo7.addActionListener(new ListenerOptionsMenu());
		modo10.addActionListener(new ListenerOptionsMenu());
		reset.addActionListener(new ListenerOptionsMenu());
		
		modo4.setSelected(true);
		modo4.setEnabled(false);
		
		pointsMenu.add(modo4);
		pointsMenu.add(modo7);
		pointsMenu.add(modo10);
		optionsMenu.add(pointsMenu);
		optionsMenu.add(createOptionsContinuityMenu());
		optionsMenu.add(new JSeparator());
		optionsMenu.add(reset);
		
		return optionsMenu;
	}
	
	private JMenu createOptionsContinuityMenu() {
		
		JMenu continuityMenu = new JMenu("Force Continuity");
		final JCheckBoxMenuItem cont0 = new JCheckBoxMenuItem("C0G0");
		final JCheckBoxMenuItem cont1 = new JCheckBoxMenuItem("C0G1");
		final JCheckBoxMenuItem cont2 = new JCheckBoxMenuItem("C1G1");
		
		class ListenerOptionsMenu implements ActionListener
		{
			public void actionPerformed(ActionEvent event)
			{
				
				if(event.getActionCommand().equals("C0G0")) {
					cont1.setSelected(false);
					cont2.setSelected(false);
					enableOtherChoices(cont0);
					painel.changeContinuity(0);
				}
				else if(event.getActionCommand().equals("C0G1")) {
						cont0.setSelected(false);
						cont2.setSelected(false);
						enableOtherChoices(cont1);
						painel.changeContinuity(1);
					
				}
				else if(event.getActionCommand().equals("C1G1")) {
					cont0.setSelected(false);
					cont1.setSelected(false);
					enableOtherChoices(cont2);
					painel.changeContinuity(2);
				}
			}
			
			private void enableOtherChoices(JMenuItem item) {
				
				item.setEnabled(false);
				
				if(!item.equals(cont0))
					cont0.setEnabled(true);
				
				if(!item.equals(cont1))
					cont1.setEnabled(true);
				
				if(!item.equals(cont2))
					cont2.setEnabled(true);
			}
		}
		
		cont0.addActionListener(new ListenerOptionsMenu());
		cont1.addActionListener(new ListenerOptionsMenu());
		cont2.addActionListener(new ListenerOptionsMenu());
		
		cont0.setSelected(true);
		cont0.setEnabled(false);
		
		continuityMenu.add(cont0);
		continuityMenu.add(cont1);
		continuityMenu.add(cont2);
		
		return continuityMenu;
	}
	
	private JCheckBoxMenuItem criarViewBB(String texto) {
		
		JCheckBoxMenuItem bb = new JCheckBoxMenuItem(texto);
		bb.setSelected(false);
		
		class ListenerItemMenu implements ActionListener
		{
			public void actionPerformed(ActionEvent e) {
				painel.toggleBoundingBox();
			}
		}
		
		bb.addActionListener(new ListenerItemMenu());
		return bb;
	}
		
	private JCheckBoxMenuItem criarViewPolLine(String texto) {
		
		JCheckBoxMenuItem line = new JCheckBoxMenuItem(texto);
		line.setSelected(true);
		
		class ListenerItemMenu implements ActionListener
		{
			public void actionPerformed(ActionEvent e) {
				painel.togglePolygonalLine();
			}
		}
		
		line.addActionListener(new ListenerItemMenu());
		return line;
	}
	
	private JMenuItem criarItemMenuAjuda(String texto) 
	{
		JMenuItem item = new JMenuItem(texto);

		class ListenerItemMenu implements ActionListener
		{
			public void actionPerformed(ActionEvent e)
			{
				if(e.getActionCommand().equals("About"))
					JOptionPane.showMessageDialog(null, 
					"Programa para desenhar curvas 2d em" +
					" Java/Swing\n\n(Guilherme Fial e Frederico Alves)\n");
			}
		}
		
		item.addActionListener(new ListenerItemMenu());
		return item;
	}
	private JMenu criarMenuAjuda()
	{
		JMenu menu = new JMenu("Help");		
		
		menu.add(criarItemMenuAjuda("About"));
		return menu;
	}
	
	private XORPanel painel;
	
	private static final int WIDTH = 800;
	private static final int HEIGHT= 600;

}
