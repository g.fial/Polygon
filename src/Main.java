import javax.swing.JFrame;
/**
 * @author Guilherme Fial 42210
 * @author Frederico Alves 41713
 */
public class Main {

	public static void main(String[] args) {
		
		XORFrame frame = new XORFrame();				//Criar o frame da aplicacao
		frame.setTitle("Editor de curvas 2D"); 
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);									// Tornar o frame visivel
	}
}
