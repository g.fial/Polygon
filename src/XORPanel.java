import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Toolkit;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.Math;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import javax.swing.JPanel;
/**
 * @author Guilherme Fial 42210
 * @author Frederico Alves 41713
 */

public class XORPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private static final int ICON_SIZE = 10;
	private static final int PRINT_WIDTH = 21;
	private static final int PRINT_HEIGHT = 28;
	private static final int NO_OBJECT			= -1;
	private static final int BOX_INSIDE 		= 1;
	private static final int BOX_CORNER_L_DOWN 	= 2;
	private static final int BOX_CORNER_R_DOWN 	= 3;
	private static final int BOX_CORNER_R_UP 	= 4;
	private static final int BOX_CORNER_L_UP 	= 5;
	private static final int BOX_LEFT_SIDE 		= 6;
	private static final int BOX_RIGHT_SIDE 	= 7;
	private static final int BOX_TOP_SIDE 		= 8;
	private static final int BOX_BOTTOM_SIDE 	= 9;
	private static final Color BEZIERCOLOR1 = Color.RED.darker();
	private static final Color BEZIERCOLOR2 = Color.RED;
	private static final Color SPLINECOLOR1 = Color.GREEN.darker();
	private static final Color SPLINECOLOR2 = Color.GREEN;
	private static final Color CATROMCOLOR1 = Color.BLUE.darker();
	private static final Color CATROMCOLOR2 = Color.CYAN.darker();
	private static final String CURSOR_PATH = "src/cursors/";
	private static final double[][] BEZIER		= 	{{-1, 3,-3, 1},
		{ 3,-6, 3, 0},
		{-3, 3, 0, 0},
		{ 1, 0, 0, 0}};
	private static final double[][] SPLINE		= 	{{-(1.0/6), 0.5,-0.5, 1.0/6},
		{ 0.5,-1, 0.5, 0},
		{-0.5, 	0, 0.5, 0},
		{ 1.0/6, 4.0/6, 1.0/6, 0}};
	private static final double[][] CATMULL		= 	{{-0.5, 1.5,-1.5, 0.5},
		{ 1,-2.5, 2, -0.5},
		{-0.5, 0, 0.5, 0},
		{ 0, 1, 0, 0}};

	
	private final float dash1[] = { 10.0f };
	private final 	BasicStroke dashed = new BasicStroke(1.0f,
					BasicStroke.CAP_BUTT,
					BasicStroke.JOIN_MITER, 10.0f, dash1, 0.0f);

	public XORPanel() {

		// Fundo de cor cinzento claro
		setBackground(Color.LIGHT_GRAY);
		initializeCursors();

		MouseListener ml = new MouseAdapter() {

			public void mousePressed(MouseEvent e) {

				if (e.getButton() != MouseEvent.BUTTON1)
					return;
				if (e.getClickCount() != 1)
					return;

				// Obter a posicao actual do cursor
				int xPos = e.getX();
				int yPos = e.getY();

				// Tudo o que nao e construcao de linha
				if (!linhaPresa && count > 0) {

					int obj = getBoxObject(xPos, yPos);

					if(printing)
						setPrintingWindow(xPos, yPos);

					// Resizes
					else if(obj >= BOX_LEFT_SIDE){
						resizing = getBoxObject(xPos, yPos);
						setScale(xPos, yPos);
					}
					// Ponto
					else if((editingPointId = getCP(xPos, yPos)) > NO_OBJECT) {
						editCP(xPos, yPos, editingPointId);
					}
					// Rotate
					else if(obj <= BOX_CORNER_L_UP && obj >= BOX_CORNER_L_DOWN) {
						startMouseBasedRotation(xPos, yPos);
					}

					else if(getBoxObject(xPos, yPos) == BOX_INSIDE){
						xPosAnterior = xPos;
						yPosAnterior = yPos;
						translating = true;
					}
				}

				else if(count < seq) // Se a linha ainda esta incompleta
					createPolygonalLine(xPos, yPos);

			}

			public void mouseReleased(MouseEvent e) {

				editingPointId = NO_OBJECT;	
				resizing = NO_OBJECT;
				rotating = false;
				translating = false;
				setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
				repaint();
			}

			public void mouseExited(MouseEvent e) {
				mouseExited = true;
			}

			public void mouseEntered(MouseEvent e) {
				mouseExited = false;
			}
		};

		MouseMotionListener mml = new MouseMotionAdapter() {

			public void mouseMoved(MouseEvent e) {

				Graphics2D g = (Graphics2D) getGraphics();
				int objectOver = -1;

				// Obter a posicao actual do cursor
				int xPos = e.getX();
				int yPos = e.getY();

				if (linhaPresa && !mouseExited) {

					// Colocar em modo de desenho XOR
					g.setXORMode(getBackground());
					g.setColor(Color.YELLOW);

					// Apagar a linha antiga
					if(!comp_resized)
						g.drawLine(xPosInicial, yPosInicial, xPosAnterior, yPosAnterior);

					// Desenha a linha nova
					g.drawLine(xPosInicial, yPosInicial, xPos, yPos);

					// Memorizar a posicao actual
					xPosAnterior = xPos;
					yPosAnterior = yPos;
				}

				else if(printing) {
					
					setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));

					if(settingPWindow)
						drawPrintWindowBorders(xPos, yPos, g);
				}
				
				else if(!boundingBox) {
					
					if(getCP(xPos, yPos) != -1)
						setCursor(new Cursor(Cursor.HAND_CURSOR));
					
					else
						setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
				}

				else if((objectOver = getBoxObject(xPos, yPos)) != NO_OBJECT) {

					if(objectOver <= BOX_CORNER_L_UP && objectOver >= BOX_CORNER_L_DOWN) {
						setCursor(rotateCursor); // Usar rotate cursor
					}

					else if (objectOver == BOX_LEFT_SIDE || objectOver == BOX_RIGHT_SIDE) {
						setCursor(new Cursor(Cursor.E_RESIZE_CURSOR));
					}

					else if(objectOver == BOX_TOP_SIDE || objectOver == BOX_BOTTOM_SIDE) {
						setCursor(new Cursor(Cursor.N_RESIZE_CURSOR));
					}

					else if(getCP(xPos, yPos) != -1)
						setCursor(new Cursor(Cursor.HAND_CURSOR));

					else if(objectOver == BOX_INSIDE)
						setCursor(moveCursor);

					else
						setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
				}

				else {
					setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
				}

				comp_resized = false;
			}

			public void mouseDragged(MouseEvent e) {

				if (linhaPresa)
					return;

				int xPos = e.getX();
				int yPos = e.getY();

				if (editingPointId != NO_OBJECT)
					editCP(xPos, yPos, editingPointId);

				else if(translating) {
					translate(xPos - xPosAnterior, yPos - yPosAnterior,true);
					xPosAnterior = xPos;
					yPosAnterior = yPos;
				}

				else if(rotating)
					rotateToMouse(xPos, yPos);

				else if(resizing != NO_OBJECT)
					setScale(xPos,yPos);

			}

		};

		ComponentListener cl = new ComponentAdapter() {

			public void componentResized(ComponentEvent e) {
				repaint();
				comp_resized = true;
			}

		};

		addMouseListener(ml);
		addMouseMotionListener(mml);
		addComponentListener(cl);
	}


	/**
	 * Refreshes the entire JPanel sketch area.
	 */
	public void paintComponent(Graphics g) {

		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(Color.YELLOW);

		if (count > 0) {

			if(boundingBox) {

				if(rotating || translating || resizing != NO_OBJECT)
					createBoundingBox(g2,true);


				else
					createBoundingBox(g2,false);
			}

			if(continuity!=0 && editingPointId!=-1)
				calculateDotCont(editingPointId, continuity);

			for (int i = 0; i < count; i++) {

				createCP(i,g2);

				if(line && !(i == (count-1))) {
					g2.setColor(Color.YELLOW);
					g2.drawLine((int)x[i],(int)y[i],(int)x[i + 1],(int)y[i + 1]);
				}
			}

			if(!linhaPresa) {
				if(bezier)
					drawCurve(g2, BEZIER);
				if(spline)
					drawCurve(g2, SPLINE);
				if(catmull)
					drawCurve(g2, CATMULL);
			}
		}
	}

	/**
	 * Enables or disables the bounding-box.
	 */
	public void toggleBoundingBox() {
		boundingBox = !boundingBox;
		repaint();
	}

	/**
	 * Enables or disables the Bezier curves.
	 */
	public void toggleBezier() {
		bezier = !bezier;
		repaint();
	}

	/**
	 * Enables or disables the B-Spline curves.
	 */
	public void toggleBSpline() {
		spline = !spline;
		repaint();
	}

	/**
	 * Enables or disables the Catmull-Rom curves.
	 */
	public void toggleCatmull() {
		catmull = !catmull;
		repaint();
	}

	/**
	 * Changes the curves continuity.
	 * @param cont
	 */
	public void changeContinuity(int cont){
		continuity = cont;
	}

	/**
	 * Clears the sketch, causing the application to return to it's initial
	 * state.
	 */
	public void clear() {

		count = 0;
		seq = nextSequence;
		linhaPresa = false;
		x = new double[seq];
		y = new double[seq];
		BBox = new double[4];
		repaint();
	}

	/**
	 * Toggles polygonal line visibility.
	 */
	public void togglePolygonalLine() {

		line = !line;
		repaint();
	}

	/**
	 * Sets the number of control points that can be placed.
	 * It can still set the number in the middle of a polygonal line
	 * creation if the number of points placed is still lower than 
	 * the wished value.
	 * @param number - Number of points
	 */
	public void setSequence(int number) {

		if(!linhaPresa && count > 0 || count > (number-1)) {
			nextSequence = number;
			clear();
		}

		else {
			seq = number;
			nextSequence = number;

			// Caso o num. de pontos ainda criados seja inferior a uma nova decisao
			// de alterar os pontos a criar (para mais), aqui isso e alterado.
			double[] aux = new double[seq];
			double[] aux2 = new double[seq];

			for(int i = 0; i < x.length && i < number; i++) {
				aux[i] = x[i];
				aux2[i] = y[i];
			}

			x = aux;
			y = aux2;
		}
	}

	/**
	 * Ensures all conditions are met to apply mouse position based
	 * rotation to the sketch.
	 * @param xp - Mouse X position when initiating rotation
	 * @param yp - Mouse Y position when initiating rotation
	 * @return Boolean that shows whether rotation started or not.
	 */
	public boolean startMouseBasedRotation(int xp, int yp) {

		if(linhaPresa || count == 0)
			rotating = false;

		else {
			double[] middle = getMiddlePos();
			previousAngle = getAngle(xp,yp,middle[0],middle[1]);
			rotating = true;
			xCenter = getMiddlePos()[0];
			yCenter = getMiddlePos()[1];
			repaint();
		}

		return rotating;
	}

	/**
	 * Enables the creation of printing window so printing can be done after
	 **/
	public void startPrinting() {

		if(!linhaPresa && count != 0)
			printing = true;

	}

	/**
	 * Prints the inside of the previously defined printing window.
	 **/
	public void print() {

		FileWriter fstream;
		
		xCenter = Math.abs(printWindow[0]+printWindow[2])/2;
		yCenter = Math.abs(printWindow[1]+printWindow[3])/2;
		
		double fact;
		
		// X scale factor
		fact = (double)PRINT_WIDTH/(double)Math.abs(printWindow[2] - printWindow[0]);
		
		// If Y has a greater factor than X
		if(fact > (double)PRINT_HEIGHT/(double)Math.abs(printWindow[3] - printWindow[1]))
			fact = (double)PRINT_HEIGHT/(double)Math.abs(printWindow[3] - printWindow[1]);
		
		
		try {
			fstream = new FileWriter("print.ps");
			BufferedWriter out = new BufferedWriter(fstream);
			out.write("%!PS");
			out.newLine();
			out.write("/cm {28.35 mul} def");
			out.newLine();
			out.write(fitPrintX(x[0],fact) + " cm "
				+ fitPrintY(y[0],fact) + " cm " + " moveto");
			out.newLine();
			out.write("gsave");
			out.newLine();
			
			if(line){
				
				for (int i = 1; i < x.length; i++) {
					out.newLine();
					out.write(fitPrintX(x[i],fact) + " cm "
							+ fitPrintY(y[i],fact) + " cm "
							+ " lineto");
				}
			}
			
			out.newLine();
			out.write("[0.2 cm 0.2 cm] 0 setdash");
			out.newLine();
			out.write("0.02 cm setlinewidth \n 1.0 0.0 0.0 setrgbcolor \n stroke");
			out.newLine();
			out.write("grestore");
			out.newLine();
			
			out.write("gsave");
			out.newLine();
			
			out.write(fitPrintX(printWindow[0],fact) + " cm "
					+ fitPrintY(printWindow[1],fact) + " cm "
					+ " moveto");
			
			out.newLine();
			out.write(fitPrintX(printWindow[0],fact) + " cm "
					+ fitPrintY(printWindow[3],fact) + " cm "
					+ " lineto");
			
			out.newLine();
			out.write(fitPrintX(printWindow[2],fact) + " cm "
					+ fitPrintY(printWindow[3],fact) + " cm "
					+ " lineto");
			
			out.newLine();
			out.write(fitPrintX(printWindow[2],fact) + " cm "
					+ fitPrintY(printWindow[1],fact) + " cm "
					+ " lineto");
			out.newLine();
			out.write("closepath");
			out.newLine();
			out.write("0.03 cm setlinewidth \n 0.0 1.0 0.0 setrgbcolor \n stroke");
			out.newLine();
			out.write("grestore");
			out.newLine();
			
			if (bezier) {
				for (int i = 1; i < 4; i++) {
					out.write(fitPrintX(x[i],fact) + " cm ");
					out.write(fitPrintY(y[i],fact) + " cm ");
				}
				out.write("curveto");
				out.newLine();


				if(x.length >4){
					for (int i = 3; i < 7; i++) {
						out.write(fitPrintX(x[i],fact) + " cm ");
						out.write(fitPrintY(y[i],fact) + " cm ");
					}
					out.write("curveto");
					out.newLine();
				}
				
				if(x.length >7){
					for (int i = 6; i < 10; i++) {
						out.write(fitPrintX(x[i],fact) + " cm ");
						out.write(fitPrintY(y[i],fact) + " cm ");
					}
					out.write("curveto");
					out.newLine();
				}
				
				

				out.write("stroke");
				out.newLine();
			}
			out.write("showpage");
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Translates "xp" position into printing coordinates.
	 * @param xp - X value to be transformed
	 * @param fact - xp width factor
	 **/
	private double  fitPrintX(double xp, double fact) {

		double r = (((xp-xCenter) * fact) + (PRINT_WIDTH/2));
		return r;
	}

	/**
	 * Translates "yp" position into printing coordinates.
	 * @param yp - Y value to be transformed
	 * @param fact - yp height factor
	 **/
	private double fitPrintY(double yp, double fact) {

		double r = (((yp-yCenter)*(-fact))+(PRINT_HEIGHT/2));
		return r;
	}

	/**
	 * Starts and ends the creation of the print screen window.
	 * Upon first call it will create one initial corner of the window,
	 * the following calls will keep drawing the window borders as the user 
	 * moves the mouse to specify the opposing corner until one call carries 
	 * the click flag at true, which means that the corner is to be defined 
	 * at the current position terminating the window editing and sending 
	 * the information to print.
	 * @param xp - Mouse X position
	 * @param yp - Mouse Y position
	 */
	private void setPrintingWindow(int xp, int yp) {

		if(!printing)
			return;

		if(!settingPWindow) { //first corner
			printWindow[0] = xp;
			printWindow[1] = yp;
			printWindow[2] = xp;
			printWindow[3] = yp;
			settingPWindow = true;
		}

		else { //last corner
			printWindow[2] = xp;
			printWindow[3] = yp;
			settingPWindow = false;
			print();
			printing = false;
		}
	}

	/**
	 * Draws the printing window borders and deletes the previous in XOR mode.
	 * @param xp - current Mouse X position
	 * @param yp - current Mouse Y position
	 * @param g - JavaSwing 2D Graphics
	 */
	private void drawPrintWindowBorders(int xp, int yp, Graphics2D g) {

		if(!settingPWindow)
			return;

		g.setXORMode(getBackground());
		g.setColor(Color.RED);

		// Delete previous
		g.drawLine((int)printWindow[0],(int)printWindow[1],(int)printWindow[2],(int)printWindow[1]);
		g.drawLine((int)printWindow[0],(int)printWindow[1],(int)printWindow[0],(int)printWindow[3]);
		g.drawLine((int)printWindow[0],(int)printWindow[3],(int)printWindow[2],(int)printWindow[3]);
		g.drawLine((int)printWindow[2],(int)printWindow[1],(int)printWindow[2],(int)printWindow[3]);

		// Create new
		g.drawLine((int)printWindow[0],(int)printWindow[1],xp,(int)printWindow[1]);
		g.drawLine((int)printWindow[0],(int)printWindow[1],(int)printWindow[0],yp);
		g.drawLine((int)printWindow[0],yp,xp,yp);
		g.drawLine(xp,(int)printWindow[1],xp,yp);

		printWindow[2] = xp;
		printWindow[3] = yp;
	}

	/**
	 * Draws a curve based on the matrix in the input.
	 * @param g - JavaSwing 2D Graphics
	 * @param matrix - Matrix that defines the curve to be drawn.
	 */
	private void drawCurve(Graphics2D g, double[][] matrix) {

		if(linhaPresa)
			return;

		g.setStroke(new BasicStroke(2));
		int startP = 0; //ponto de comeco para a curva
		float tStep = 0.01f;
		float t = 0.00f;
		double lastX = 0, lastY = 0;
		double curveX = 0, curveY = 0, lastCurveX = 0, lastCurveY = 0;
		double[] T = {tStep*tStep*tStep, tStep*tStep, tStep, 1};
		double[] Ax = new double[4], Ay = new double[4]; //A = M.G

		while(startP < x.length - 3) {

			t = 0;

			// Definir a cor dos trocos
			if(matrix == BEZIER) {
				if(startP % 2 == 0)
					g.setColor(BEZIERCOLOR1);
				else
					g.setColor(BEZIERCOLOR2);
			}

			else if(matrix == SPLINE) {
				if(startP % 2 == 0)
					g.setColor(SPLINECOLOR1);
				else
					g.setColor(SPLINECOLOR2);
			}

			else {
				if(startP % 2 == 0)
					g.setColor(CATROMCOLOR1);
				else
					g.setColor(CATROMCOLOR2);
			}

			// A construir matriz A de cada um dos 4 pontos
			for(int i = 0; i < 4; i++) {
				for(int j = 0; j < 4; j++) {
					Ax[i] = (matrix[i][j] * x[j+startP]) + lastX;
					Ay[i] = (matrix[i][j] * y[j+startP]) + lastY;
					lastX = Ax[i];
					lastY = Ay[i];
				}
				lastX = 0;
				lastY = 0;
			}

			if(matrix == BEZIER)
				startP += 3;

			else
				startP += 1;

			while(t <= 1.0) {

				T[0] = t*t*t;
				T[1] = t*t;
				T[2] = t;
				T[3] = 1;

				for(int j = 0; j < 4; j++) {
					curveX = (Ax[j] * T[j]) + lastX;
					curveY = (Ay[j] * T[j]) + lastY;
					lastX = curveX;
					lastY = curveY;
				}

				if((t == 0 && startP - 1 == 0) || (t == 0 && startP - 3 == 0)) {
					lastCurveX = curveX;
					lastCurveY = curveY;
				}

				g.drawLine((int)lastCurveX,(int)lastCurveY,(int)curveX,(int)curveY);

				lastCurveX = lastX;
				lastCurveY = lastY;
				lastX = 0;
				lastY = 0;
				t += tStep;
			}
		}
	}

	/**
	 * Applies the rotation transformation.
	 * The sketch will rotate to be constantly facing the mouse cursor's
	 * (xPos,yPos) position.
	 * @param xPos - mouse X position
	 * @param yPos - mouse Y position
	 */
	private void rotateToMouse(double xPos, double yPos) {

		if(mouseExited)
			return;
		
		double xAux, yAux, angle, angleDif;

		translate(-xCenter, -yCenter, false); // Fazer rotacao na origem
		angle = getAngle(xPos, yPos, xCenter, yCenter);
		angleDif = angle - previousAngle;
		previousAngle = angle;

		for(int i = 0; i < seq; i++) {
			xAux = x[i];
			yAux = y[i];
			x[i] = xAux*Math.cos(angleDif) - yAux*Math.sin(angleDif);
			y[i] = xAux*Math.sin(angleDif) + yAux*Math.cos(angleDif);
		}

		translate(xCenter, yCenter, true);
	}

	/**
	 * Applies the translating transformation to the sketch.
	 * The whole sketch will move applying (dx,dy) to every point.
	 * @param dx
	 * @param dy
	 * @param Specifies if this translation will show on the screen.
	 */
	private void translate(double dx, double dy, boolean visible) {
		
		if(mouseExited)
			translating = false;

		for(int i = 0; i < seq; i++) {
			x[i] = x[i] + dx;
			y[i] = y[i] + dy;
		}

		if(visible)
			repaint();

	}

	/**
	 * Applies the scaling transformation on the sketch uniformly.
	 * The sketch will scale its size so the wall goes under (xPos,yPos).
	 * @param xPos
	 * @param yPos
	 */
	private void setScale(double xPos, double yPos) {

		if(mouseExited)
			return;

		double[] middle = getMiddlePos();
		double xfactor, yfactor, xp, yp;

		xp = xPos - middle[0];
		yp = yPos - middle[1];
		yfactor = Math.abs(yp/(BBox[3]-middle[1]));
		xfactor = Math.abs(xp/(BBox[2]-middle[0]));

		// Se largura ou altura forem menores que 1 pixel, ignorar.
		if(Math.abs(xp) < 1 || Math.abs(yp) < 1)
			return;

		// Centrar na origem
		translate(-middle[0],-middle[1],false);

		if(resizing == BOX_LEFT_SIDE || resizing == BOX_RIGHT_SIDE) {

			for(int i = 0; i < x.length; i++) {
				x[i] = x[i] * xfactor;
				y[i] = y[i] * xfactor;
			}
		}

		else if(resizing == BOX_BOTTOM_SIDE || resizing == BOX_TOP_SIDE){

			for(int i = 0; i < y.length; i++) {
				y[i] = y[i] * yfactor;
				x[i] = x[i] * yfactor;
			}
		}

		translate(middle[0], middle[1],true);
		repaint();

	}

	/**
	 * Initiates or continues the creation of the polygonal line.
	 * @param xPos
	 * @param yPos
	 */
	private void createPolygonalLine(int xPos, int yPos) {

		if (linhaPresa) { // Estamos a acabar uma linha...
			// Usar a caneta amarela

			x[count] = xPos; //Guardar coordenadas do ponto
			y[count] = yPos;
			count++;
		}

		if (count < seq) { // Estamos a iniciar uma linha e cria
			// ponto
			if(count == 0){ // Primeiro ponto

				x[count] = xPos; //Guardar coordenadas do ponto a cabeca
				y[count] = yPos;
				count++;
			}

			linhaPresa = true;
			xPosInicial = xPosAnterior = xPos;
			yPosInicial = yPosAnterior = yPos;
		}

		else { // reset count, acabou seq
			linhaPresa = false;
		}

		repaint();
	}

	/**
	 * Creates the bounding-box on the screen and updates values on memory.
	 * @param g - JavaSwing2D Graphics
	 * @param editing - Specifies if the BB is undergoing a transformation
	 */
	private void createBoundingBox(Graphics2D g, boolean editing) {

		if(linhaPresa || count == 0)
			return;

		g.setColor(Color.GRAY);

		for(int i = 0; i < x.length; i++) {

			if(i == 0){
				BBox[2] = x[i];
				BBox[3] = y[i];
				BBox[0] = x[i];
				BBox[1] = y[i];
			}

			if(BBox[2] < x[i])
				BBox[2] = x[i];

			if(BBox[0] > x[i])
				BBox[0] = x[i];

			if(BBox[3] < y[i])
				BBox[3] = y[i];

			if(BBox[1] > y[i])
				BBox[1] = y[i];

		}

		if(editing)
			g.setStroke(dashed);

		g.drawRect((int)BBox[0],(int)BBox[1],(int)(BBox[2]-BBox[0]),
				(int)(BBox[3]-BBox[1]));

		g.setStroke(new BasicStroke());
	}

	/**
	 * Updates the control point's position to (xPos, yPos).
	 * @param xPos - new X position
	 * @param yPos - new Y position
	 * @param id - identification/number of the control point
	 */
	private void editCP(int xPos, int yPos, int id) {

		if(mouseExited)
			return;
		
		if(id == 0) { // Ponto extremidade P0

			x[id] = xPos;
			y[id] = yPos;	
		}	

		// Ponto extremidade P3, P6 ou P9
		else if (id <= x.length - 1) {

			x[id] = xPos;
			y[id] = yPos;	
		}

		// Pontos no meio
		else {

			x[id] = xPos;
			y[id] = yPos;
		}

		repaint();
	}

	/**
	 * Shows which control point is under (xPos,yPos).
	 * @param xPos
	 * @param yPos
	 * @return Integer corresponding to control point's id or NO_OBJECT
	 */
	private int getCP(int xPos, int yPos) {

		for(int i = 0; i < count; i++){

			if(xPos <= x[i]+ICON_SIZE*0.55 && xPos >= x[i]-ICON_SIZE*0.55){
				if(yPos <= y[i]+ICON_SIZE*0.55 && yPos >= y[i]-ICON_SIZE*0.55){
					return i;
				}
			}
		}

		return NO_OBJECT;
	}

	/**
	 * Shows which zone of the bounding-box is under (xPos, yPos).
	 * @param xPos
	 * @param yPos
	 * @return Integer code of the bounding-box's zone
	 */
	private int getBoxObject(int xPos, int yPos) {

		if (!boundingBox) // BoundingBox desativada
			return NO_OBJECT;

		if (xPos > BBox[0] && xPos < BBox[2] && yPos > BBox[1] && yPos < BBox[3])
			return BOX_INSIDE;

		// Lado esquerdo
		else if (xPos <= BBox[0] && xPos >= BBox[0]-ICON_SIZE) {

			if(yPos >= BBox[1]-ICON_SIZE && yPos <= BBox[1])
				return BOX_CORNER_L_UP;

			else if (yPos >= BBox[3] && yPos <= BBox[3]+ICON_SIZE)
				return BOX_CORNER_L_DOWN;

			else if(yPos > BBox[1] && yPos < BBox[3] && xPos > BBox[0]-(ICON_SIZE/2))
				return BOX_LEFT_SIDE;
		}

		// Lado direito
		else if (xPos >= BBox[2] && xPos <= BBox[2]+ICON_SIZE) {

			if(yPos >= BBox[3] && yPos <= BBox[3]+ICON_SIZE)
				return BOX_CORNER_R_DOWN;

			else if(yPos >= BBox[1]-ICON_SIZE && yPos <= BBox[1])
				return BOX_CORNER_R_UP;

			else if(yPos > BBox[1] && yPos < BBox[3] && xPos < BBox[2]+(ICON_SIZE/2))
				return BOX_RIGHT_SIDE;
		}

		else if(xPos > BBox[0] && xPos < BBox[2]) {

			// Cima
			if(yPos <= BBox[1] && yPos > BBox[1]-(ICON_SIZE/2))
				return BOX_TOP_SIDE;

			// Baixo
			else if(yPos >= BBox[3] && yPos < BBox[3]+(ICON_SIZE/2))
				return BOX_BOTTOM_SIDE;
		}

		return 0;
	}

	/**
	 * This function creates a control point (x[id],y[id]).
	 * @param id - control point's sequence number/ identification
	 * @param g - JavaSwing 2D Graphics
	 */
	private void createCP(int id, Graphics2D g) {

		g.setColor(Color.BLUE); //Cor

		g.drawRect((int)(x[id]-(ICON_SIZE*0.5)), (int)(y[id]-(ICON_SIZE*0.5)),
				ICON_SIZE, ICON_SIZE); //Ponto
		String s = "P" + id; //Nome do ponto

		//Desenhar nome
		g.drawChars(s.toCharArray(), 0, s.length(),(int)(x[id]-(ICON_SIZE*0.5))
				, (int)(y[id]-(ICON_SIZE*0.5)-4));
	}

	/**
	 * This function obtains the angle between (xPos,yPos) and (Ox,Oy).
	 * @param xPos - X
	 * @param yPos - Y
	 * @param Ox - X from considered origin
	 * @param Oy - Y from considered origin
	 * @return Angle in RAD ranging from 0 to 2*PI
	 */
	private double getAngle(double xPos, double yPos, double Ox, double Oy) {

		double angle,h;

		h = (Math.sqrt(Math.pow(xPos-Ox,2) + Math.pow(yPos-Oy,2)));

		if(xPos >= Ox && yPos >= Oy) { // 1 quadrante
			angle = Math.acos((xPos-Ox)/h);
		}

		else if(xPos < Ox && yPos > Oy) { // 2 quad
			angle = Math.acos((yPos-Oy)/h) + (Math.PI/2);
		}

		else if(xPos < Ox && yPos <= Oy) { // 3 quad
			angle = Math.asin((xPos-Ox)/h) + 3*(Math.PI/2);
		}

		else { // 4 quad
			angle = Math.asin((yPos-Oy)/h) + 2*(Math.PI);
		}

		return angle;
	}

	/**
	 * This function calculates the center of the bounding-box.
	 * @return double[2] array, where [0] has X value and [1] has Y values
	 */
	private double[] getMiddlePos() {

		if(count == 0) {
			return null;
		}

		double maxX = 0, maxY = 0, minX = 0, minY = 0;
		double[] middle = new double[2];

		for(int i = 0; i < x.length; i++) {

			if(i == 0){
				minX = x[i];
				minY = y[i];
				maxX = x[i];
				maxY = y[i];
			}

			if(maxX <= x[i])
				maxX = x[i];

			if(minX > x[i])
				minX = x[i];

			if(maxY <= y[i])
				maxY = y[i];

			if(minY > y[i])
				minY = y[i];

		}

		middle[0] = Math.sqrt(Math.pow(minX - maxX, 2));
		middle[1] = Math.sqrt(Math.pow(minY - maxY, 2));

		// Se x1 < x2, somar a X a metade da diferenca
		if(minX <= maxX) {
			middle[0] = middle[0]/2;
			middle[0] = minX + middle[0];
		}

		// Se x1 > x2, subtrair a X a metade da diferenca
		else {
			middle[0] = middle[0]/2;
			middle[0] = minX - middle[0];
		}

		// Se y1 < y2, somar a Y a metade da diferenca
		if(minY <= maxY) {
			middle[1] = middle[1]/2;
			middle[1] = minY + middle[1];
		}

		// Se y1 > y2, subtrair a Y a metade da diferenca
		else {
			middle[1] = middle[1]/2;
			middle[1] = minY - middle[1];
		}

		return middle;
	}

	/**
	 * Calculates the distance from (x,y) to (x2,y2).
	 * @param x
	 * @param y
	 * @param x2
	 * @param y2
	 * @return Double, distance.
	 */
	private double distance(double x, double y, double x2, double y2){
		return Math.sqrt(Math.pow(x-x2, 2)+Math.pow(y-y2, 2));
	}

	/**
	 * This function realigns the necessary control points to ensure continuity.
	 * @param index - control point number
	 * @param continuity - type of continuity
	 */
	private void calculateDotCont(int index, int continuity){

		if((index%3) == 0 || index < 2 || index > x.length-3 || continuity==0)
			return;

		double xa, ya, xp, yp;
		double xb = x[index];
		double yb = y[index];
		int alterIndex;

		if((editingPointId - 1)%3 == 0){ //Se P4, P7...
			xa = x[index-2];
			ya = y[index-2];
			xp = x[index-1];
			yp = y[index-1];
			alterIndex = index-2;
		}

		else{ //Se P2, P5...
			xa = x[index+2];
			ya = y[index+2];
			xp = x[index+1];
			yp = y[index+1];
			alterIndex = index+2;
		}

		double k1 = distance(xa, ya, xp, yp);
		double k2 = k1;


		if(continuity == 1)
			k2 = distance(xb,yb,xp,yp);
		
		xa = ((k1*xp)+(k2*xp)-(k1*xb))/(k2);
		ya = ((k1*yp)+(k2*yp)-(k1*yb))/(k2);
			
		if(k1 != 0 && k2 != 0){
			x[alterIndex] = xa;
			y[alterIndex] = ya;
		}
		
	}
	
	/**
	 * Creates the custom cursors.
	 */
	private void initializeCursors() {
		 
		Toolkit tk = Toolkit.getDefaultToolkit();
		 try{
			 
			 rotateCursor = tk.createCustomCursor(tk.getImage
					 (CURSOR_PATH+"rotate.png"),new Point(11,11),"Rotate Cursor");
			 
			 moveCursor = tk.createCustomCursor(tk.getImage
					 (CURSOR_PATH+"move.png"),new Point(13,13),"Move Cursor");
			
		 }catch(Exception e){
			 rotateCursor = new Cursor(Cursor.DEFAULT_CURSOR);
			 moveCursor = new Cursor(Cursor.DEFAULT_CURSOR);
		 }
	}

	private int continuity = 0;
	private boolean linhaPresa, comp_resized, boundingBox, mouseExited, translating;
	private boolean rotating, line = true, bezier, catmull, spline, printing;
	private boolean settingPWindow;
	private int seq = 4, nextSequence = 4;
	private int editingPointId = -1;
	private int resizing = NO_OBJECT, count = 0;
	private double previousAngle = 0;
	private double[] BBox = new double[4]; // {x1,y1,x2,y2}
	private Integer[] printWindow = new Integer[4]; // {x1,y1,x2,y2}
	private double[] x = new double[seq]; // Guarda a pos. X do obj. i
	private double[] y = new double[seq]; // Guarda a pos. Y do obj. i
	private double xCenter, yCenter;
	private int xPosInicial, yPosInicial;
	private int xPosAnterior, yPosAnterior;
	private Cursor rotateCursor, moveCursor;
}
